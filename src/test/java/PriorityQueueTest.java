import java.util.PriorityQueue;

/**
 * 优先队列是一个很实用的编程工具，put进来的元素会自动地排序放到合适的位置，保证每次poll的值都是优先级最高的元素
 * 在均匀划分并行排序中，我利用了优先队列来实现多个数组的合并操作
 *
 * @see PriorityQueue
 */
public class PriorityQueueTest {
    public static void main(String[] args) {
        PriorityQueue<Integer> queue = new PriorityQueue<>();
        queue.add(9);
        queue.add(8);
        queue.add(4);
        queue.add(6);
        queue.add(3);
        queue.add(7);
        while (!queue.isEmpty()) {
            System.out.println(queue.poll());
        }
        /*
        result is:
        3
        4
        6
        7
        8
        9
         */
    }
}
