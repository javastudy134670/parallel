package com.luo.parallel.hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

public class MyBellmanFord {
    static String startNode;
    static String workingDir;
    static FileSystem fs;
    static int jobId;

    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        Configuration configuration = new Configuration();
        fs = FileSystem.get(configuration);
        String[] otherArgs = (new GenericOptionsParser(configuration, args)).getRemainingArgs();
        if (otherArgs.length < 2) {
            System.err.println("Usage: my-bellman-ford <start_node> <workdir> ");
            System.exit(2);
        }
        System.out.println("got args" + Arrays.toString(otherArgs));
        startNode = otherArgs[0];
        workingDir = otherArgs[1];
        Path inputPath = new Path(workingDir + "/input");
        Path tmpPath = new Path(workingDir + "/tmp");
        Job job;
        boolean continueFlag = true;
        for (jobId = 1; continueFlag; ++jobId) {
            System.out.printf("job%d started=========================%n", jobId);
            job = Job.getInstance(configuration, "job" + jobId);
            job.setJarByClass(MyBellmanFord.class);
            job.setMapperClass(BellmanFordMapper.class);
            job.setCombinerClass(BellmanFordCombiner.class);
            job.setReducerClass(BellmanFordReducer.class);
            job.setOutputKeyClass(Text.class);
            job.setOutputValueClass(DoubleWritable.class);
            FileInputFormat.addInputPath(job, inputPath);
            FileOutputFormat.setOutputPath(job, tmpPath);
            job.waitForCompletion(true);
            fs.msync();
            FSDataInputStream inputStream = fs.open(new Path(workingDir + "/tmp/part-r-00000"));
            if (inputStream.readAllBytes().length == 0) {
                continueFlag = false;
            }
            inputStream.close();
            fs.delete(tmpPath, true);
            System.out.printf("job%d finished========================%n", jobId);
        }
        FSDataOutputStream outputStream = fs.create(new Path(workingDir + "/result.txt"));
        RemoteIterator<LocatedFileStatus> files = fs.listFiles(new Path(workingDir + "/out"), false);
        while (files.hasNext()) {
            LocatedFileStatus file = files.next();
            FSDataInputStream inputStream = fs.open(file.getPath());
            outputStream.write(inputStream.readAllBytes());
            inputStream.close();
        }
        outputStream.close();
    }

    public static class BellmanFordMapper extends Mapper<LongWritable, Text, Text, DoubleWritable> {
        Text node = new Text();
        DoubleWritable outputDistance = new DoubleWritable();
        String hostname = InetAddress.getLocalHost().getHostName();

        public BellmanFordMapper() throws UnknownHostException {
        }

        @Override
        protected void setup(Mapper<LongWritable, Text, Text, DoubleWritable>.Context context) {
            System.out.printf("host:%s job%d mapper setup...%n", hostname, jobId);
        }

        @Override
        protected void cleanup(Mapper<LongWritable, Text, Text, DoubleWritable>.Context context) {
            System.out.printf("host:%s job%d mapper cleanup...%n", hostname, jobId);
        }

        @Override
        protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, DoubleWritable>.Context context) throws IOException, InterruptedException {
            System.out.printf("host:%s job%d mapper map(%s)...%n".formatted(InetAddress.getLocalHost().getHostName(), jobId, value));
            String[] key_value = value.toString().split("\t");
            String[] values = key_value[1].split(":");
            Double src_distance = readDistance(key_value[0]);
            Double dst_distance = readDistance(values[0]);
            if (src_distance != null) {
                double new_distance = src_distance + Double.parseDouble(values[1]);
                if (dst_distance == null || new_distance < dst_distance) {
                    node.set(values[0]);
                    outputDistance.set(new_distance);
                    context.write(node, outputDistance);
                    System.out.printf("\thost:%s job%d mapper map(%s) write(%s, %f)...%n".formatted(hostname, jobId, value, node, new_distance));
                }
            }
        }
    }

    public static class BellmanFordCombiner extends Reducer<Text, DoubleWritable, Text, DoubleWritable> {
        DoubleWritable distance = new DoubleWritable();
        String hostname = InetAddress.getLocalHost().getHostName();

        public BellmanFordCombiner() throws UnknownHostException {
        }

        @Override
        protected void setup(Reducer<Text, DoubleWritable, Text, DoubleWritable>.Context context) {
            System.out.printf("host:%s job%d combiner setup...%n", hostname, jobId);
        }

        @Override
        protected void cleanup(Reducer<Text, DoubleWritable, Text, DoubleWritable>.Context context) {
            System.out.printf("host:%s job%d combiner cleanup...%n", hostname, jobId);
        }

        @Override
        protected void reduce(Text key, Iterable<DoubleWritable> values, Reducer<Text, DoubleWritable, Text, DoubleWritable>.Context context) throws IOException, InterruptedException {
            System.out.printf("host:%s job%d combiner reduce(%s)...%n", hostname, jobId, key);
            double min = Double.MAX_VALUE;
            for (DoubleWritable value : values) {
                min = Math.min(value.get(), min);
            }
            distance.set(min);
            context.write(key, distance);
            System.out.printf("\thost:%s job%d combiner reduce(%s) write(%f)...%n", hostname, jobId, key, min);
        }
    }

    public static class BellmanFordReducer extends Reducer<Text, DoubleWritable, Text, DoubleWritable> {
        DoubleWritable distance = new DoubleWritable();
        String hostname = InetAddress.getLocalHost().getHostName();

        public BellmanFordReducer() throws UnknownHostException {
        }

        @Override
        protected void setup(Reducer<Text, DoubleWritable, Text, DoubleWritable>.Context context) {
            System.out.printf("host:%s job%d reducer setup...%n", hostname, jobId);
        }

        @Override
        protected void cleanup(Reducer<Text, DoubleWritable, Text, DoubleWritable>.Context context) {
            System.out.printf("host:%s job%d reducer cleanup...%n", hostname, jobId);
        }

        @Override
        protected void reduce(Text key, Iterable<DoubleWritable> values, Reducer<Text, DoubleWritable, Text, DoubleWritable>.Context context) throws IOException, InterruptedException {
            System.out.printf("host:%s job%d reducer reduce(%s)...%n", hostname, jobId, key);
            double min = Double.MAX_VALUE;
            for (DoubleWritable value : values) {
                min = Math.min(value.get(), min);
            }
            distance.set(min);
            saveDistance(key.toString(), min);
            System.out.printf("\thost:%s job%d reducer reduce(%s) save(%f)...%n", hostname, jobId, key, min);
            context.write(key, distance);
        }
    }

    static Double readDistance(String nodeName) throws IOException {
        Path path = new Path(workingDir + "/out/" + nodeName);
        if (fs.exists(path)) {
            FSDataInputStream inputStream = fs.open(new Path(workingDir + "/out/" + nodeName));
            String all = new String(inputStream.readAllBytes());
            inputStream.close();
            return Double.parseDouble(all.split("\t")[1]);
        }
        return null;
    }

    static void saveDistance(String nodeName, double distance) throws IOException {
        FSDataOutputStream outputStream = fs.create(new Path(workingDir + "/out/" + nodeName));
        String content = nodeName + "\t" + distance + "\n";
        outputStream.write(content.getBytes());
        outputStream.close();
    }
}
