import com.luo.parallel.basic.ArrayAddition;

import java.util.Random;

public class ArrayAdditionTest {
    public static void main(String[] args) throws InterruptedException {
        Random random = new Random(System.currentTimeMillis());
        int length = 50000000;
        int[] dst = new int[length];
        int[] src = new int[length];
        for (int i = 0; i < dst.length; i++) {
            dst[i] = random.nextInt();
            src[i] = random.nextInt();
        }
        int[] dst_copy = dst.clone();
        normalAddition(dst, src);
        for (int i = 1; i <= 12; i++) {
            parallelAddition(dst, src, i);
        }
    }

    static void normalAddition(int[] dst, int[] src) {
        long time = System.currentTimeMillis();
        for (int i = 0; i < dst.length; i++) {
            dst[i] += src[i];
        }
        time = System.currentTimeMillis() - time;
//        for (int i : dst) {
//            System.out.println(i);
//        }
        System.out.printf("normal addition finished... (%d ms)%n", time);
    }

    static void parallelAddition(int[] dst, int[] src, int p) throws InterruptedException {
        long time = System.currentTimeMillis();
        ArrayAddition.addition(dst, src, 0, dst.length, p);
        time = System.currentTimeMillis() - time;
//        for (int i : dst) {
//            System.out.println(i);
//        }
        System.out.printf("parallel (p=%d) addition finished... (%d ms)%n", p, time);
    }
}

