package com.luo.parallel.basic;

import java.util.concurrent.CountDownLatch;

/**
 * 并行数组相加
 * <p>将数组均匀划分成p份，交给多个子线程进行相加操作。这些操作不会引发冲突所以不需要同步互斥
 * <p>最后，让主线程等待各个子线程完成任务
 */
public class ArrayAddition {
    /**
     * 并p个线程的并行数组相加
     * @param dst 被加数组
     * @param src 另一个数组
     * @param from 涉及到加法的开始元素下标
     * @param to 涉及到加法的最后一个元素下标+1
     * @param p 线程数量
     * @throws InterruptedException 同步工具CountDownLatch的await操作可能会引发中断异常
     */
    public static void addition(int[] dst, int[] src, int from, int to, int p) throws InterruptedException {
        if (from < 0) throw new RuntimeException("from>=0 is required");
        if (from >= to) throw new RuntimeException("from<to is required");
        if (to > Math.min(dst.length, src.length)) throw new RuntimeException("to out of bounds");
        CountDownLatch latch = new CountDownLatch(p);
        for (int i = 0; i < p; i++) {
            final int thread_id = i;
            new Thread(() -> {
                int start = from + (to - from) * thread_id / p;
                int end = from + (to - from) * (thread_id + 1) / p;
                for (int j = start; j < end; j++) {
                    dst[j] = src[j] + dst[j];
                }
                latch.countDown();
            }).start();
        }
        latch.await();
    }
}
