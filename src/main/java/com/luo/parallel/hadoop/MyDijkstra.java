package com.luo.parallel.hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.chain.ChainReducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

public class MyDijkstra {
    public MyDijkstra() {
    }

    static String startNode;
    static String workingDir;
    static String nearestNode = "Begin";
    static double nearestDistance;
    private static final String TEMP_NODE = "#TEMP_NODE";

    public static void main(String[] args) throws Exception {
        Configuration configuration = new Configuration();
        String[] otherArgs = (new GenericOptionsParser(configuration, args)).getRemainingArgs();
        if (otherArgs.length < 2) {
            System.err.println("Usage: my-dijkstra <start_node_name> <working_dir>");
            System.exit(2);
        }
        startNode = otherArgs[0];
        workingDir = otherArgs[1];
        int step = 1;
        Job job;
        while (!nearestNode.isEmpty()) {
            nearestDistance = Double.MAX_VALUE;
            nearestNode = "";
            System.out.printf("prepare to begin job%d..%n", step);
            job = Job.getInstance(configuration, "job%d finder".formatted(step));
            job.setJarByClass(MyDijkstra.class);
            job.setMapperClass(FindMapper.class);
            job.setCombinerClass(FindCombiner.class);
            job.setReducerClass(FindReducer.class);
            job.setOutputValueClass(Text.class);
            job.setOutputKeyClass(Text.class);
            FileInputFormat.addInputPath(job, new Path("%s/step%d".formatted(workingDir, step)));
            FileOutputFormat.setOutputPath(job, new Path("%s/step%d/tmp".formatted(workingDir, step)));
            job.waitForCompletion(true);
            System.out.println("nearestNode=" + nearestNode);
            System.out.printf("prepare to next step job%d..%n", step);
            job = Job.getInstance(configuration, "job%d next step".formatted(step));
            job.setJarByClass(MyDijkstra.class);
            job.setMapperClass(NextStepMapper.class);
            job.setCombinerClass(NextStepReducer.class);
            job.setReducerClass(NextStepReducer.class);
            job.setOutputValueClass(Text.class);
            job.setOutputKeyClass(Text.class);
            FileInputFormat.addInputPath(job, new Path("%s/step%d/tmp".formatted(workingDir, step)));
            FileOutputFormat.setOutputPath(job, new Path("%s/step%d".formatted(workingDir, step + 1)));
            job.waitForCompletion(true);
            ++step;
        }
        System.out.println("Final Work");
        job = Job.getInstance(configuration, "final work");
        job.setJarByClass(MyDijkstra.class);
        job.setMapperClass(FinalMapper.class);
        ChainReducer.setReducer(job, FinalReducer.class, Text.class, Text.class, Text.class, Text.class, configuration);
        ChainReducer.addMapper(job, Mapper.class, Text.class, Text.class, Text.class, Text.class, configuration);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path("%s/step%d".formatted(workingDir, step)));
        FileOutputFormat.setOutputPath(job, new Path("%s/output".formatted(workingDir)));
        job.waitForCompletion(true);
        System.out.println("Finish");
    }

    public static class FindMapper extends Mapper<Object, Text, Text, Text> {
        Text temp_node = new Text(TEMP_NODE);
        Text node_key = new Text();
        Text node_value = new Text();

        public FindMapper() {
        }

        @Override
        public void map(Object key, Text value, Mapper<Object, Text, Text, Text>.Context context) throws IOException, InterruptedException {
            System.out.println(FindMapper.class);
            String[] key_value = value.toString().split("\t");
            System.out.println(Arrays.toString(key_value));
            node_key.set(key_value[0]);
            node_value.set(key_value[1]);
            String[] strings = key_value[1].split(":");
            if (strings[0].equals(key_value[0])) return; //去除自环
            if (key_value[0].equals(startNode) && strings.length == 2) {
                System.out.println("Come from map to temp_node");
                context.write(temp_node, node_value);
            }
            context.write(node_key, node_value);
        }
    }

    public static class FindCombiner extends Reducer<Text, Text, Text, Text> {
        Text nearestText = new Text();

        public FindCombiner() {
        }

        @Override
        public void reduce(Text key, Iterable<Text> values, Reducer<Text, Text, Text, Text>.Context context) throws IOException, InterruptedException {
            System.out.println(FindCombiner.class);
            if (key.toString().equals(TEMP_NODE)) {
                System.out.println("Come from combine to temp_node");
                double locallyMinDistance = Double.MAX_VALUE;
                for (Text value : values) {
                    double distance = Double.parseDouble(value.toString().split(":")[1]);
                    if (distance < locallyMinDistance) {
                        locallyMinDistance = distance;
                        nearestText.set(value);
                    }
                }
                context.write(key, nearestText);
            } else {
                for (Text value : values) {
                    context.write(key, value);
                }
            }
        }
    }

    public static class FindReducer extends Reducer<Text, Text, Text, Text> {
        public FindReducer() {
        }

        @Override
        public void reduce(Text key, Iterable<Text> values, Reducer<Text, Text, Text, Text>.Context context) throws IOException, InterruptedException {
            System.out.println(FindReducer.class);
            if (key.toString().equals(TEMP_NODE)) {
                for (Text value : values) {
                    String[] strings = value.toString().split(":");
                    double distance = Double.parseDouble(strings[1]);
                    if (distance < nearestDistance) {
                        nearestDistance = distance;
                        nearestNode = strings[0];
                    }
                }
            } else {
                for (Text value : values) {
                    context.write(key, value);
                }
            }
        }
    }

    public static class NextStepMapper extends Mapper<LongWritable, Text, Text, Text> {
        Text textKey = new Text();
        Text textValue = new Text();

        public NextStepMapper() {
        }

        @Override
        public void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, Text>.Context context) throws IOException, InterruptedException {
            System.out.println(NextStepMapper.class);
            System.out.println("key=" + key + " value=" + value);
            String[] key_value = value.toString().split("\t");
            System.out.println("key_value=" + Arrays.toString(key_value));
            String[] strings = key_value[1].split(":");
            System.out.println("strings=" + Arrays.toString(strings));
            if (key_value[0].equals(nearestNode)) {
                textKey.set(startNode);
                textValue.set(strings[0] + ":" + (nearestDistance + Double.parseDouble(strings[1])));
            } else if (key_value[0].equals(startNode) && strings[0].equals(nearestNode)) {
                textKey.set(key_value[0]);
                textValue.set(key_value[1] + ":pass");
            } else {
                textKey.set(key_value[0]);
                textValue.set(key_value[1]);
            }
            context.write(textKey, textValue);
        }
    }

    public static class NextStepReducer extends Reducer<Text, Text, Text, Text> {
        public NextStepReducer() {
        }

        @Override
        public void reduce(Text key, Iterable<Text> values, Reducer<Text, Text, Text, Text>.Context context) throws IOException, InterruptedException {
            System.out.println(NextStepReducer.class);
            if (key.toString().equals(startNode)) {
                System.out.println("enter startNode from next step reducer");
                HashMap<String, Text> map = new HashMap<>();
                for (Text value : values) {
                    System.out.println("key=" + key + " value=" + value);
                    String[] strings = value.toString().split(":");
                    double distance = Double.parseDouble(strings[1]);
                    Text DstTextOnMap = map.get(strings[0]);
                    System.out.println("DstTextOnMap = " + DstTextOnMap);
                    if (DstTextOnMap == null || Double.parseDouble(DstTextOnMap.toString().split(":")[1]) > distance) {
                        System.out.println("map.put(strings[0], value); strings[0]=" + strings[0] + " value=" + value);
                        map.put(strings[0], new Text(value));
                    }
                }
                System.out.println(map);
                for (Text value : map.values()) {
                    context.write(key, value);
                }
            } else {
                for (Text value : values) {
                    System.out.println("key=" + key + " value=" + value);
                    context.write(key, value);
                }
            }
        }
    }

    public static class FinalMapper extends Mapper<LongWritable, Text, Text, Text> {
        Text writeKey = new Text();
        Text writeValue = new Text();

        @Override
        public void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, Text>.Context context) throws IOException, InterruptedException {
            String[] key_value = value.toString().split("\t");
            String[] strings = key_value[1].split(":");
            if (key_value[0].equals(startNode)) {
                writeKey.set(strings[0]);
                writeValue.set(strings[1]);
            } else {
                writeKey.set(key_value[0]);
                writeValue.set("unreachable");
            }
            context.write(writeKey, writeValue);
        }
    }

    public static class FinalReducer extends Reducer<Text, Text, Text, Text> {
        @Override
        public void reduce(Text key, Iterable<Text> values, Reducer<Text, Text, Text, Text>.Context context) throws IOException, InterruptedException {
            for (Text value : values) {
                context.write(key, value);
                if (value.toString().equals("unreachable")) {
                    break;
                }
            }
        }
    }

}
