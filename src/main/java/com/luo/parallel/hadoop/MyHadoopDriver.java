package com.luo.parallel.hadoop;

import org.apache.hadoop.util.ProgramDriver;

/**
 * Class Loader
 * Enable to choose various of programs to execute
 *
 * @author LuoHao
 */
public class MyHadoopDriver {
    public static void main(String[] args) {
        int exitCode = -1;
        long time = -200;
        ProgramDriver programDriver = new ProgramDriver();
        try {
            programDriver.addClass("my-wordcount", MyWordCount.class, "this is a wordcount program dividing words with ':'");
            programDriver.addClass("my-dijkstra", MyDijkstra.class, "parallel dijkstra");
            programDriver.addClass("my-bellman-ford", MyBellmanFord.class, "parallel bellman-ford");
            time = System.currentTimeMillis();
            exitCode = programDriver.run(args);
            time = System.currentTimeMillis() - time;
        } catch (Throwable throwable) {
            throwable.fillInStackTrace();
        }
        System.out.printf("Program finished as code: %d after %d ms%n", exitCode, time);
        System.exit(exitCode);
    }
}
