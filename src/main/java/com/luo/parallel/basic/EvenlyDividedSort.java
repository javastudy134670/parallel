package com.luo.parallel.basic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.concurrent.CountDownLatch;

/**
 * 均匀划分并行排序
 * <p>先把数组均匀划分成p份，交给p个子线程
 * <p>然后每个子线程排序，抽取p个样本元素，交给主线程 (sampleCollectStage())
 * <p>然后主线程将样本元素排序，再抽出p-1个标志点元素
 * <p>然后每个子线程根据p-1个标志点元素，进一步分成p组。一共得到p*p组。 (groupingStage())
 * <p>每份的第一组交给线程一合并，每份的第二组交给线程二合并，以此类推 (mergeStage())
 * <p>所有子线程合并完成后，主线程将得到排序最终结果
 * <p>整个过程可以分成以上六个步骤，每个步骤都是同步的，步骤2~5之间需要信号量实现同步，因此一个需要4个同步信号量。
 */
public class EvenlyDividedSort {
    /**
     * 均匀划分并行排序
     *
     * @param array 被排序的数组
     * @param p     线程个数
     * @param c     比较器 这个排序方法是支持并行的，只要有对应的比较器Comparator就可以
     * @param from  排序涉及到的开始元素下标
     * @param to    排序涉及到的结束元素下标+1
     * @param <T>   支持泛型
     * @throws InterruptedException CountDownLatch的await()可能会引发中断异常
     */

    @SuppressWarnings("unchecked")
    public static <T> void sort(T[] array, int p, Comparator<? super T> c, int from, int to) throws InterruptedException {
        if (p <= 0) throw new RuntimeException("p > 0 is required");
        if (from < 0) throw new RuntimeException("from>=0 is required");
        if (from >= to) throw new RuntimeException("from<to is required");
        if (to > array.length) throw new RuntimeException("to<=array.length is required");
        Object[] sample = new Object[p * p];
        int[][] index = new int[p][];
        ArrayList<T>[] results = new ArrayList[p];
        CountDownLatch sampleCollectLatch = new CountDownLatch(p);
        CountDownLatch sampleSortLatch = new CountDownLatch(1);
        CountDownLatch groupingLatch = new CountDownLatch(p);
        CountDownLatch mergeLatch = new CountDownLatch(p);
        for (int i = 0; i < p; i++) {
            final int thread_id = i;
            new Thread() {
                final int start = from + thread_id * (to - from) / p;
                final int end = from + (thread_id + 1) * (to - from) / p;

                @Override
                public void run() {
                    try {
                        sampleCollectStage();
                        sampleCollectLatch.countDown();
                        sampleSortLatch.await();
                        groupingStage();
                        groupingLatch.countDown();
                        groupingLatch.await();
                        mergeStage();
                        mergeLatch.countDown();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }

                void sampleCollectStage() {
                    Arrays.sort(array, start, end, c);
                    for (int k = 0; k < p; k++) sample[thread_id * p + k] = array[start + (end - start) * k / p];
                }

                void groupingStage() {
                    index[thread_id] = new int[p + 1];
                    index[thread_id][0] = start;
                    for (int i0 = 1; i0 < p; i0++) {
                        index[thread_id][i0] = Arrays.binarySearch(array, start, end, sample[p * i0]);
                        if (index[thread_id][i0] < 0) index[thread_id][i0] = -1 - index[thread_id][i0];
                    }
                    index[thread_id][p] = end;
                }

                void mergeStage() {
                    int[] each_index = new int[p];
                    results[thread_id] = new ArrayList<>();
                    PriorityQueue<Integer> priorityQueue = new PriorityQueue<>((t1, t2) -> c.compare(array[each_index[t1]], array[each_index[t2]]));
                    for (int i0 = 0; i0 < p; i0++) {
                        each_index[i0] = index[i0][thread_id];
                        if (each_index[i0] < index[i0][thread_id + 1]) {
                            priorityQueue.add(i0);
                        }
                    }
                    while (!priorityQueue.isEmpty()) {
                        int peek = priorityQueue.poll();
                        results[thread_id].add(array[each_index[peek]]);
                        each_index[peek]++;
                        if (each_index[peek] < index[peek][thread_id + 1]) {
                            priorityQueue.add(peek);
                        }
                    }
                }
            }.start();
        }
        sampleCollectLatch.await();
        Arrays.sort((T[]) sample, c);
        sampleSortLatch.countDown();
        mergeLatch.await();
        for (int i = from; i < to; ) {
            for (int j = 0; j < p; j++) {
                for (T t : results[j]) {
                    array[i++] = t;
                }
            }
        }
    }

}
