# Java Parallel

Author: LuoHao

一些并行计算的练习。

com.luo.parallel.basic.ArrayAddition: 并行数组相加

com.luo.parallel.basic.EvenlyDividedSort: 平均划分并行排序

com.luo.parallel.hadoop.MyWordCount: hadoop单词计数，单词之间用冒号隔开

com.luo.parallel.hadoop.MyDijkstra: hadoop实现dijkstra算法，找最短路径
