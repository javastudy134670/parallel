import com.luo.parallel.basic.EvenlyDividedSort;

import java.util.*;

public class EvenlyDividedSortTest {
    public static void main(String[] args) throws InterruptedException {
        Random random = new Random(System.currentTimeMillis());
        List<Integer> list = new ArrayList<>();
        int length = 10000000;
        System.out.printf("length = %d%n", length);
        for (int i = 0; i < length; i++) {
            list.add(random.nextInt());
        }
        normalSortTest(list.toArray(list.toArray(new Integer[0])));
        for (int i = 1; i <= 12; i++) {
            list.clear();
            for (int j = 0; j < length; j++) {
                list.add(random.nextInt());
            }
            evenlySortTest(list.toArray(new Integer[0]), i);
        }
    }

    public static void normalSortTest(Integer[] array) {
//        System.out.println("normal sort start");
        long time = System.currentTimeMillis();
        Arrays.sort(array);
        time = System.currentTimeMillis() - time;
//        for (Integer integer : array) {
//            System.out.println(integer);
//        }
        System.out.printf("normal sort end..(%d ms)%n", time);
    }

    public static void evenlySortTest(Integer[] array, int p) throws InterruptedException {
//        System.out.printf("evenly sort start, p = %d%n", p);
        long time = System.currentTimeMillis();
        EvenlyDividedSort.sort(array, p, Comparator.naturalOrder(), 0, array.length);
        time = System.currentTimeMillis() - time;
//        for (Integer integer : array) {
//            System.out.println(integer);
//        }
        System.out.printf("evenly(p=%d) sort end..(%d ms)%n", p, time);
    }
}
